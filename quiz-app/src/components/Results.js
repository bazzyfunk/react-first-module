import React from "react";
import tally from "../helpers/tally";

const Results = ({ userAnswers, score, restartQuiz }) => {
  const triesTotal = tally(userAnswers);
  const oneTry = triesTotal[1] && (
    <div>
      <strong>{triesTotal[1]}</strong> du premier coup (tu es si fort).
    </div>
  );
  const twoTries = triesTotal[2] && (
    <div>
      <strong>{triesTotal[2]}</strong> au second essai.
    </div>
  );
  const threeTries = triesTotal[3] && (
    <div>
      <strong>{triesTotal[3]}</strong> en trois.
    </div>
  );
  const fourTries = triesTotal[4] && (
    <div>
      <strong>{triesTotal[4]}</strong> en quatre.
    </div>
  );

  return (
    <div className="results-container">
      <h2>Résultat du questionnaire</h2>
      <div>Tu as répondu</div>
      {oneTry}
      {twoTries}
      {threeTries}
      {fourTries}
      <div className="results-total">
        Ton score total est <strong>{score}</strong>.
      </div>
      <button onClick={restartQuiz}>Recommencer</button>
    </div>
  );
};

export default Results;
