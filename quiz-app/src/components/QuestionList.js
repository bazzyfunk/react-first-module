import React from "react";
import Question from "./Question";

const QuestionList = ({ questions, handleAnswerClick }) => {
  return (
    <ul className="question-list">
      {questions.map(question => {
        return (
          <Question
            key={question.question.props.children.toString()}
            question={question.question}
            answers={question.answers}
            handleAnswerClick={handleAnswerClick}
          />
        );
      })}
    </ul>
  );
};

export default QuestionList;
