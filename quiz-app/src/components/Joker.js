import React, { useState } from "react";
import styled from "styled-components";

export default () => {
  const [Joker, updateJoker] = useState(false);

  const Box = styled.div`
    margin: 0 auto;
    width: 450px;
    font-size: 2em;
    display: flex;
    flex-direction: column;
    text-align: center;
    padding: 30px 0;
  `;

  let used = "";
  if (Joker === true) {
    used = "Bien entendu Marcel !";
  } else {
    used = "Est-ce votre ultime bafouille ?";
  }
  return (
    <Box>
      <button onClick={() => updateJoker(!Joker)}>
        Est-ce que tu as la référence ?
      </button>
      <span>{used}</span>
    </Box>
  );
};
