import React from "react";
import QuestionList from "./QuestionList";
import styled from "styled-components";
import Joker from "./Joker";

const Quiz = ({
  step,
  questions,
  totalQuestions,
  score,
  handleAnswerClick
}) => {
  const QuestionCount = styled.div`
    margin-left: 100px;
  `;

  return (
    <div className="wrapper">
      <header>
        <QuestionCount>
          <h2>Question</h2>
          <div className="question-number">{step}</div>
          <div className="description">
            sur <span>{totalQuestions}</span>
          </div>
        </QuestionCount>
        <h1>Qui veut gagner de l'argent en masse ?</h1>
        <div className="score-container">
          <h2>Total</h2>
          <div className="score">{score}</div>
          <div className="description">points</div>
        </div>
      </header>

      <div className="questions">
        <QuestionList
          questions={questions}
          handleAnswerClick={handleAnswerClick}
        />
        <Joker />
      </div>
    </div>
  );
};

export default Quiz;
