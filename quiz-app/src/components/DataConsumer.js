import React from "react";
import DataContext from "../context/DataContext";

const DataConsumer = () => {
  return (
    <div>
      <DataContext.Consumer>
        {data => {
          const { value } = data;
          return <div className="content">{value}</div>;
        }}
      </DataContext.Consumer>
    </div>
  );
};

export default DataConsumer;
