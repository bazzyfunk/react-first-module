import React, { Component } from "react";
import styled from "styled-components";
import QuizApp from "./components/QuizApp";
import DataContext from "./context/DataContext";

export default class App extends Component {
  state = {
    data: null
  };

  componentDidMount() {
    this.getNewQuote();
  }

  getNewQuote() {
    fetch("https://api.chucknorris.io/jokes/random")
      .then(response => response.json())
      .then(data => this.setState({ data: data.value }));
  }

  render() {
    if (!this.state.data) return <div>En attente des données...</div>;

    const { data } = this.state;

    const Quote = styled.div`
      text-align: center;
      margin: 0 auto;
      font-size: 2em;
      width: 50%;
    `;

    return (
      <DataContext.Provider value={data}>
        <QuizApp totalQuestions={4} />
        <Quote>
          <div>
            <strong>
              Petit indice (dans la langue de Secoue-lance tabernac)
            </strong>
          </div>
          {data}
        </Quote>
      </DataContext.Provider>
    );
  }
}
