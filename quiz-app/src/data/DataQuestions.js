import React from "react";

const QUESTION_DATA = [
  {
    question: (
      <span>
        Lorsqu’un pancake tombe dans la neige avant le 31 décembre, on dit :
      </span>
    ),
    answers: [
      <span>
        Que c’est un pancake qui est tombé dans la neige avant le 31 décembre
      </span>,
      <span>Que c’est une Kippa surgelée</span>,
      <span>Que c’est un frisbee comestible</span>,
      <span>La réponse D</span>
    ],
    correct: 2
  },
  {
    question: (
      <span>
        Lorsqu’un pancake prend l’avion à destination de Toronto, et qui s’en va
        faire une escale technique à St Claude, qui c’est qu’on va dire de ce
        pancake là ?
      </span>
    ),
    answers: [
      <span>Qu’il est pas encore arrivé à Toronto</span>,
      <span>
        Qu’il est disposé à arriver à Toronto, mais qu’on l’attend toujours
      </span>,
      <span>Qui c’est qu’il fout ce maudit pancake tabernac’</span>,
      <span>La réponse D</span>
    ],
    correct: 1
  },
  {
    question: (
      <span>
        Lorsqu’un pancake est invité à une Barmitzva, est ce qu’on doit :
      </span>
    ),
    answers: [
      <span>L’inciter à aller boire à l’open Barmitzva</span>,
      <span>Lui offrir et lui présenter Raymond Barmitzva</span>,
      <span>Lui présenter des malabarmitzvas</span>,
      <span>La réponse D</span>
    ],
    correct: 0
  },
  {
    question: (
      <span>Au cours de quel évènement historique, fut créé le Pancake ?</span>
    ),
    answers: [
      <span>En 1618 pendant la guerre des croissants au beurre</span>,
      <span>Pendant le massacre de la Saint Panini</span>,
      <span>
        En 112 avant Céline, pendant la prise de la brioche… évènement tragique
      </span>,
      <span>La réponse D</span>
    ],
    correct: 1
  }
];

export default QUESTION_DATA;
