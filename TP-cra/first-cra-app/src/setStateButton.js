import React, { useState } from "react";

export default function Button() {
  // DONE set initial state with initial buttonText
  const [buttonText, updateText] = useState("Click here !");

  return (
    // DONE update state to change buttonText on click
    <button onClick={() => updateText("Clicked !")}>{buttonText}</button>
  );
}
