import React from "react";
import useFetch from "./useFetch";

export default function DataLoader(props) {
  // Récupérer la liste des bière utilisant votre fonction js useFetch
  const data = useFetch("https://api.punkapi.com/v2/beers");

  return (
    <div>
      <ul>
        <h1>THIS IS BEERS</h1>
        {data.map(el => (
          <li key={el.id}>{el.name}</li>
        ))}
      </ul>
    </div>
  );
}
