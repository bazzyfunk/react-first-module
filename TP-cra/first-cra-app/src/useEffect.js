import React, { useState, useEffect } from "react";

export default function DataLoader() {
  // Set data as a state
  const [dataState, updateData] = useState([]);

  useEffect(() => {
    fetch("https://randomuser.me/api")
      .then(response => response.json())
      .then(data => updateData(data.results[0]));
  }, []);
  // Call https://randomuser.me/api

  return (
    <div>
      {dataState.name && dataState.name.first + " " + dataState.name.last}
    </div>
  );
}
