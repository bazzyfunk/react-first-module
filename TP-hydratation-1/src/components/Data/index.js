import React, {Component} from 'react';
import DataContext from '../../context/DataContext';
import DataConsumer from '../DataConsumer';
import styled from 'styled-components';

export default class Data extends Component {
  state = {
    data: null,
  };

  componentDidMount() {
    this.getNewBeer();
  }

  getNewBeer = () => {
    fetch('https://api.punkapi.com/v2/beers/random')
        .then(response => response.json())
        .then(data => this.setState({data: data[0]}));
  };

  render() {
    if (!this.state.data) return <div>En attente des données...</div>;

    const {data} = this.state;

    const Block = styled.div`
      background-color: pink;
      margin: 0 auto;
      width: auto;
      height: auto;
    `;

    return (
      <DataContext.Provider value={data}>
        <Block>
          <DataConsumer />
        </Block>
      </DataContext.Provider>
    );
  }
}
