import React from 'react';
import DataContext from '../../context/DataContext';
import styled, {css} from 'styled-components';

const Name = styled.h1`
  font-size: 2rem;
  ${props => css`
    color: ${props.color};
  `}
`;

const DataConsumer = () => {
  return (
    <div>
      <DataContext.Consumer>
        {data => {
          const {name, description, image_url} = data;
          return (
            <div className="content">
              <Name color="green">{name}</Name>
              <p>{description}</p>
              <img src={image_url} />
            </div>
          );
        }}
      </DataContext.Consumer>
    </div>
  );
};

export default DataConsumer;
