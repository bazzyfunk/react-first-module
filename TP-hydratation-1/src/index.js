import React from 'react';
import ReactDOM from 'react-dom';
import './styles/global.scss';
import Data from './components/Data';

const app = <Data />;

const ReactRoot = document.getElementById('react-root');
ReactDOM.render(app, ReactRoot);
