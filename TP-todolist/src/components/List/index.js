import React, {Component} from 'react';
import Item from '../Item';

export default class List extends Component {
  render() {
    return (
      <div className="list">
        <Item
          className="item"
          items={this.props.items}
          onClick={this.props.deleteSingle}
          done={this.props.done}
        />
      </div>
    );
  }
}
