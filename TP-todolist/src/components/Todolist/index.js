import React, {Component} from 'react';
import localStorage from 'localStorage';
import List from '../List';

class Todolist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      items: [
        {
          title: 'Hey jude',
          isDone: false,
        },
      ],
    };
  }

  componentDidUpdate() {
    localStorage.setItem('items', JSON.stringify(this.state.items));
  }

  componentDidMount() {
    const storedItems = JSON.parse(localStorage.getItem('items'));
    if (storedItems != null) {
      this.setState({items: storedItems});
    } else {
      this.setState({items: this.state.items});
      localStorage.setItem('items', JSON.stringify(this.state.items));
    }
  }

  sendTask = event => {
    const newValue = this.state.value;
    this.state.items.push({title: newValue, isDone: false});
    this.setState({value: ''});
    event.preventDefault();
  };

  handleInput = event => {
    this.setState({value: event.target.value});
  };

  deleteAll = () => {
    this.setState({items: []});
  };

  deleteSingle = (index, event) => {
    this.state.items.splice(index, 1);
    const newItems = this.state.items;
    this.setState({items: newItems});
    event.preventDefault();
  };

  done = (index, item) => {
    const allItems = this.state.items;
    allItems[index].isDone = !item.isDone;
    this.setState({
      items: allItems,
    });
  };

  render() {
    return (
      <div className="content">
        <h1>Ma magnifique TodoList</h1>
        <List items={this.state.items} deleteSingle={this.deleteSingle} done={this.done} />

        <form onSubmit={this.sendTask}>
          <input onChange={this.handleInput} value={this.state.value} />
          <button className="btn btn__submit">Submit</button>
          <button className="btn btn__delete" onClick={this.deleteAll}>
            Clear
          </button>
        </form>
      </div>
    );
  }
}

export default Todolist;
