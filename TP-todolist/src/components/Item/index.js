import React from 'react';

const Item = props => {
  return props.items
      .sort((a, b) => b.isDone - a.isDone)
      .map((item, index) => (
        <div className={item.isDone ? 'item item__done' : 'item item__todo'} key={index}>
          <span>{item.title}</span>
          <div className="btns">
            <button className="btn btn__confirm" onClick={props.done.bind(this, index, item)}>
            Done
            </button>
            <button className="btn btn__delete" onClick={props.onClick.bind(this, index)}>
            Delete
            </button>
          </div>
        </div>
      ));
};

export default Item;
