import React from 'react';
import ReactDOM from 'react-dom';
import Todolist from './components/Todolist';
import './styles/global.scss';

const app = <Todolist />;

const ReactRoot = document.getElementById('react-root');
ReactDOM.render(app, ReactRoot);
